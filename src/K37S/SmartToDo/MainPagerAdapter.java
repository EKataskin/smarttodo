package k37s.SmartToDo;

import k37s.SmartToDo.Birthday.FragmentBirthdayList;
import k37s.SmartToDo.Loan.FragmentLoanList;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.TimingLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Адаптер, который отвечает за предоставление данных для ViewPager.
 * Унаследован от стандартной (частичной) реализации абстрактного PagerAdapter, которая работает с фрагментами.
 * Для реализации остается только создать фрагмент и определить кол-во страниц.
 */
public class MainPagerAdapter extends FragmentPagerAdapter
{
	private static final Logger logger=LoggerFactory.getLogger( MainPagerAdapter.class );

	private static final String[] TAB_TITLES=new String[]
	{
		SmartToDoApp.getAppContext().getString( R.string.tabHotEvents ),
		SmartToDoApp.getAppContext().getString( R.string.tabToDo ),
		SmartToDoApp.getAppContext().getString( R.string.tabBirthdays ),
		SmartToDoApp.getAppContext().getString( R.string.tabLoan ),
		SmartToDoApp.getAppContext().getString( R.string.tabShopping )
	};

	public MainPagerAdapter( FragmentManager fm )
	{
		super( fm );
	}

	@Override
	public Fragment getItem( int position )
	{
		TimingLogger timing=new TimingLogger( "TIME", "GetNewPage" );
		try
		{
			switch( position )
			{
				case 2: // Для 3-ей закладки возвращаем фрагмент со списком дней рождения.
					return new FragmentBirthdayList();
				case 3: // Для 4-ой закладки возвращаем фрагмент со списком кредитов.
					return new FragmentLoanList();
				default:
					return TestFragment.newInstance( TAB_TITLES[position % TAB_TITLES.length] );
			}
		}
		finally
		{
			timing.dumpToLog();
		}
	}

	@Override
	public CharSequence getPageTitle( int position )
	{
		String title=TAB_TITLES[position % TAB_TITLES.length];
		logger.debug( "Page title requested [position={}; title={}].", position, title );
		return title;
	}

	@Override
	public int getCount()
	{
		return TAB_TITLES.length;
	}
}
