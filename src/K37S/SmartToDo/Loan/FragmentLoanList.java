package k37s.SmartToDo.Loan;

import k37s.SmartToDo.Business.Loan;
import k37s.SmartToDo.DAL.DatabaseAdapter;
import k37s.SmartToDo.R;
import k37s.SmartToDo.SmartToDoApp;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.*;

import java.util.List;

/** Фрагмент - список кредитов. */
public class FragmentLoanList extends ListFragment
{
	private DatabaseAdapter _dbAdapter;
	private ListAdapterLoan _listAdapter;

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		// Предупреждаем фреймворк, что данный фрагмент хочет создать своё меню в ActionBar.
		setHasOptionsMenu( true );
		// Загружаем свой кастомный layout списка из xml (вместо дефолтного однострочечного).
		return inflater.inflate( R.layout.list_loan, container, false );
	}

	@Override
	public void onActivityCreated( Bundle savedInstanceState )
	{
		super.onActivityCreated( savedInstanceState );

		_dbAdapter=((SmartToDoApp)getActivity().getApplicationContext()).getDbAdapter();
		_listAdapter=new ListAdapterLoan( getActivity() );
		setListAdapter( _listAdapter );

		// Инициализируем Loader.
		getLoaderManager().initLoader( 0, null, new LoaderCallbacks() );
	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater )
	{
		inflater.inflate( R.menu.menu_loan_list, menu );
	}

	private class LoaderCallbacks implements LoaderManager.LoaderCallbacks<List<Loan>>
	{
		@Override
		/** Instantiate and return a new Loader for the given ID. */
		public Loader<List<Loan>> onCreateLoader( int i, Bundle bundle )
		{
			// Создаём новый Loader, который будет загружать список кредитов.
			return new ListLoaderLoan( getActivity(), _dbAdapter );
		}

		@Override
		/** Called when a previously created loader has finished its load. */
		public void onLoadFinished( Loader<List<Loan>> loansLoader, List<Loan> loans )
		{
			_listAdapter.setLoans( loans );
			// The listview now displays the queried data.
		}

		@Override
		/** Called when a previously created loader is being reset, thus making its data unavailable. */
		public void onLoaderReset( Loader<List<Loan>> loansLoader )
		{
			// For whatever reason, the Loader's data is now unavailable.
			// Remove any references to the old data by replacing it with a null Cursor.
			_listAdapter.setLoans( null );
		}
	}
}
