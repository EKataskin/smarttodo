package k37s.SmartToDo.Loan;

import k37s.SmartToDo.Business.Loan;
import k37s.SmartToDo.Business.LoanPayment;
import k37s.SmartToDo.DateHelper;
import k37s.SmartToDo.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;

/** Адаптер для списка кредитов. */
public class ListAdapterLoan extends BaseAdapter
{
	LayoutInflater _layoutInflater;
	List<Loan> _loans;

	public void setLoans( List<Loan> loans )
	{
		_loans=loans;
		notifyDataSetChanged();
	}

	ListAdapterLoan( Context context )
	{
		_layoutInflater=LayoutInflater.from( context );
	}

	@Override
	public int getCount()
	{
		return _loans==null ? 0 : _loans.size();
	}

	@Override
	public Object getItem( int position )
	{
		return _loans==null ? null : _loans.get( position );
	}

	@Override
	public long getItemId( int position )
	{
		return _loans==null ? -1 : _loans.get( position ).getId();
	}

	/** Возвращает заполненный view для элемента списка */
	@Override
	public View getView( int position, View convertView, ViewGroup parent )
	{
		// Используем созданные, но не используемые view
		View view=convertView;
		if( view==null )
			view=_layoutInflater.inflate( R.layout.row_loan, parent, false );

		Loan loan=getLoan( position );

		// Наименование кредита
		((TextView)view.findViewById( R.id.loan_name )).setText( loan.getName() );

		// Баланс
		NumberFormat currencyFormat=NumberFormat.getCurrencyInstance();
		currencyFormat.setCurrency( loan.getCurrency() );
		if( loan.isUseBalance() )
		{
			view.findViewById( R.id.loan_balance_row ).setVisibility( View.VISIBLE );
			((TextView)view.findViewById( R.id.loan_balance_value )).setText( currencyFormat.format( loan.getBalance() ) );
		}
		else
		{
			view.findViewById( R.id.loan_balance_row ).setVisibility( View.GONE );
		}

		// Следующий платеж
		LoanPayment nextPayment=loan.getNextPayment();
		TextView nextPaymentView=(TextView)view.findViewById( R.id.loan_next_payment );
		if( nextPayment.getSum()==0 )
			nextPaymentView.setText( "" );
		else
		{
			String nextPaymentDateStr=DateHelper.FormatShortDate( nextPayment.getDate() );
			String nextPaymentSumStr=currencyFormat.format( -nextPayment.getSum() );
			nextPaymentView.setText( nextPaymentDateStr+" - "+nextPaymentSumStr );
		}

		return view;
	}

	/**
	 * Возвращает кредит из списка по позиции
	 * @param position позиция в списке
	 * @return кредит
	 */
	Loan getLoan( int position )
	{
		return (Loan)getItem( position );
	}
}