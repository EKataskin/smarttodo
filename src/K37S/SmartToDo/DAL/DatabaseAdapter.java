package k37s.SmartToDo.DAL;

import k37s.SmartToDo.Business.Loan;
import k37s.SmartToDo.Business.LoanPayment;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.util.ArrayMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/** Обёртка для доступа к БД. */
public class DatabaseAdapter
{
	private static final Logger logger=LoggerFactory.getLogger( "DatabaseAdapter" );
	private final Context _context;
	private DatabaseOpenHelper _dbHelper;
	private SQLiteDatabase _database;
	
	// Объектный пул для врапперов. Каждый враппер существует в единственном экземпляре.
	private Map<String, IObjectWrap<?>> _wrapCache=new ArrayMap<String, IObjectWrap<?>>();

	/**
	 * Конструктор
	 * @param context Контекст
	 */
	public DatabaseAdapter( Context context )
	{
		_context=context;
	}

	public DatabaseAdapter Open() throws SQLException
	{
		_dbHelper=new DatabaseOpenHelper( _context );
		_database=_dbHelper.getWritableDatabase();
		return this;
	}

	public void Close()
	{
		_dbHelper.close();
	}
	
	private IObjectWrap<?> CreateWrapper( String classSimpleName ) throws Exception
	{
		if( classSimpleName.equals( Loan.class.getSimpleName() ) )
		{
			if( !_wrapCache.containsKey( classSimpleName ) )
				_wrapCache.put( classSimpleName, new LoanWrap( _database ) );
		}
		else if( classSimpleName.equals( LoanPayment.class.getSimpleName() ) )
		{
			if( !_wrapCache.containsKey( classSimpleName ) )
				_wrapCache.put( classSimpleName, new LoanPaymentWrap( _database ) );
		}
		else
		{
			throw new Exception( "Wrapper for class '"+classSimpleName+"' does not exist!" );
		}
		return _wrapCache.get( classSimpleName );
	}

	@SuppressWarnings( "unchecked" )
	public <T> IObjectWrap<T> GetWrapper( Class<T> objectClass )
	{
		try
		{
			return (IObjectWrap<T>)CreateWrapper( objectClass.getSimpleName() );
		}
		catch( Exception ex )
		{
			logger.error( "Error creating wrapper!", ex );
		}
		return null;
	}
}
