package k37s.SmartToDo.DAL;

import k37s.SmartToDo.Business.Loan;
import k37s.SmartToDo.Business.LoanPayment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Обёртка для класса Loan. */
public class LoanWrap extends ObjectWrap<Loan>
{
	private static String TABLE="Loan";
	private static String C_ID="_id";
	private static String C_NAME="Name";
	private static String C_CURRENCY="Currency";
	private static String C_USE_BALANCE="UseBalance";
	private static String C_INITIAL_BALANCE="InitialBalance";
	
	public LoanWrap( SQLiteDatabase database )
	{
		super( database );
	}

	@Override
	public long Insert( Loan obj )
	{
		_values.clear();
		_values.put( C_NAME, obj.getName() );
		_values.put( C_CURRENCY, obj.getCurrencyCode() );
		_values.put( C_USE_BALANCE, obj.isUseBalance() );
		_values.put( C_INITIAL_BALANCE, obj.getInitialBalance() );
		obj.setId( _database.insert( TABLE, null, _values ) );
		return obj.getId();
	}

	@Override
	public void Update( Loan obj )
	{
		// TODO: Реализовать изменение объекта.
	}

	@Override
	public void Delete( long id )
	{
		// TODO: Реализовать удаление объекта.
	}

	@Override
	public Loan GetById( long id )
	{
		// TODO: Реализовать загрузку объекта.
		return null;
	}

	@Override
	public List<Loan> LoadList( String selection, String[] selectionArgs )
	{
		Cursor cursor=_database.query( TABLE, null, selection, selectionArgs, null, null, C_ID );
		try
		{
			if( cursor==null || cursor.getCount()==0 )
				return Collections.emptyList();
			cursor.moveToFirst();

			int cIdInd=cursor.getColumnIndex( C_ID );
			int cNameInd=cursor.getColumnIndex( C_NAME );
			int cCurrInd=cursor.getColumnIndex( C_CURRENCY );
			int cUseBalInd=cursor.getColumnIndex( C_USE_BALANCE );
			int cInitBalInd=cursor.getColumnIndex( C_INITIAL_BALANCE );
			ArrayList<Loan> list=new ArrayList<Loan>();
			do
			{
				Loan loan=new Loan(
					cursor.getLong( cIdInd ),
					cursor.getString( cNameInd ),
					cursor.getString( cCurrInd ),
					cursor.getFloat( cInitBalInd ),
					cursor.getInt( cUseBalInd )>0
				);
				list.add( loan );
			}
			while( cursor.moveToNext() );
			
			LoadPaymentsForAllLoans( list );
			return list;
		}
		finally
		{
			if( cursor!=null )
				cursor.close();
		}
	}

	// region Загрузка платежей
	
	/** Загружает все платежи для указанного кредита */
	private void LoadPaymentsForOneLoan( Loan loan )
	{
		LoanPaymentWrap paymentWrap=new LoanPaymentWrap( _database );
		List<LoanPayment> payments=paymentWrap.LoadListForLoan( loan.getId() );
		List<Loan> loans=new ArrayList<Loan>( 1 );
		loans.add( loan );
		SetPaymentsForLoans( loans, payments );
	}

	/** Загружает все платежи для всех кредитов */
	private void LoadPaymentsForAllLoans( List<Loan> loans )
	{
		LoanPaymentWrap paymentWrap=new LoanPaymentWrap( _database );
		List<LoanPayment> payments=paymentWrap.LoadList( null, null );
		SetPaymentsForLoans( loans, payments );
	}
	
	private void SetPaymentsForLoans( List<Loan> loans, List<LoanPayment> payments )
	{
		for( Loan loan : loans )
		{
			for( LoanPayment payment : payments )
			{
				if( payment.getLoanId()==loan.getId() )
					loan.getPayments().add( payment );
			}
			// Сортируем платежи по датам.
			Collections.sort( loan.getPayments(), LoanPayment.COMPARE_BY_DATE );
		}
	}
	
	// endregion
}
