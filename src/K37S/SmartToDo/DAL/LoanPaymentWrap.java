package k37s.SmartToDo.DAL;

import k37s.SmartToDo.Business.LoanPayment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import hirondelle.date4j.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Обёртка для класса LoanPayment. */
public class LoanPaymentWrap extends ObjectWrap<LoanPayment>
{
	private static String TABLE="LoanPayment";
	private static String C_ID="_id";
	private static String C_LOAN="LoanId";
	private static String C_DATE="Date";
	private static String C_SUM="Sum";
	private static String C_COMMENT="Comment";

	public LoanPaymentWrap( SQLiteDatabase database )
	{
		super( database );
	}

	@Override
	public long Insert( LoanPayment obj )
	{
		_values.clear();
		_values.put( C_LOAN, obj.getLoanId() );
		_values.put( C_DATE, obj.getDate().format( "YYYY-MM-DD" ) );
		_values.put( C_SUM, obj.getSum() );
		_values.put( C_COMMENT, obj.getComment() );
		obj.setId( _database.insert( TABLE, null, _values ) );
		return obj.getId();
	}

	@Override
	public void Update( LoanPayment obj )
	{
		// TODO: Реализовать изменение объекта.
	}

	@Override
	public void Delete( long id )
	{
		// TODO: Реализовать удаление объекта.
	}

	@Override
	public LoanPayment GetById( long id )
	{
		// TODO: Реализовать загрузку объекта.
		return null;
	}

	@Override
	public List<LoanPayment> LoadList( String selection, String[] selectionArgs )
	{
		Cursor cursor=_database.query( TABLE, null, selection, selectionArgs, null, null, C_DATE );
		try
		{
			if( cursor==null || cursor.getCount()==0 )
				return Collections.emptyList();
			cursor.moveToFirst();

			int cIdInd=cursor.getColumnIndex( C_ID );
			int cLoanInd=cursor.getColumnIndex( C_LOAN );
			int cDateInd=cursor.getColumnIndex( C_DATE );
			int cSumInd=cursor.getColumnIndex( C_SUM );
			int cCmntInd=cursor.getColumnIndex( C_COMMENT );
			ArrayList<LoanPayment> list=new ArrayList<LoanPayment>();
			do
			{
				LoanPayment payment=new LoanPayment(
					cursor.getLong( cIdInd ),
					cursor.getLong( cLoanInd ),
					new DateTime( cursor.getString( cDateInd ) ),
					cursor.getFloat( cSumInd ),
					cursor.getString( cCmntInd )
				);
				list.add( payment );
			}
			while( cursor.moveToNext() );
			return list;
		}
		finally
		{
			if( cursor!=null )
				cursor.close();
		}
	}

	public List<LoanPayment> LoadListForLoan( long loanId )
	{
		return LoadList( C_LOAN+"=?", new String[]{ String.valueOf( loanId ) } );
	}
}
