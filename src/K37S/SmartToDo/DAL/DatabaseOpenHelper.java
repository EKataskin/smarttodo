package k37s.SmartToDo.DAL;

import k37s.SmartToDo.BuildSettings;
import k37s.SmartToDo.Business.Item;
import k37s.SmartToDo.Business.Kind;
import k37s.SmartToDo.Business.Loan;
import k37s.SmartToDo.Business.LoanPayment;
import k37s.SmartToDo.R;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import hirondelle.date4j.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Helper для работы с БД. */
public class DatabaseOpenHelper extends SQLiteOpenHelper
{
	// Database is saved in the directory DATA/data/APP_NAME/databases/FILENAME.
	// DATA is the path which the Environment.getDataDirectory() method returns.
	// APP_NAME is your application name.
	// FILENAME is the name you specify in your application code for the database.

	private static final Logger logger=LoggerFactory.getLogger( "SQLiteOpenHelper" );
	private static final String DATABASE_NAME="K37SToDo";
	private static final int DATABASE_VERSION=1;

	private Context _context;

	public DatabaseOpenHelper( Context context )
	{
		super( context, DATABASE_NAME, null, DATABASE_VERSION );
		_context=context;
	}

	@Override
	/** Этот метод вызывается когда создаётся новая БД. */
	public void onCreate( SQLiteDatabase db )
	{
		// SQLiteOpenHelper creates a transaction before invoking its callback methods (onCreate, onUpgrade, and onOpen),
		// so explicit transaction control is unnecessary within those methods.
		ContentValues values=new ContentValues();

		logger.info( "Creating new database…" );

		// region Таблица Settings
		db.execSQL( _context.getString( R.string.tableSettings ) );

		// Дефолтные настройки для ToDoItem
		values.put( "Level", "Kind" );
		values.put( "ShowMissed", true );
		values.put( "BeforeDays", 5 );
		values.put( "OrderPriority", 10 );
		long ToDoKindSettingsId=db.insert( "Settings", null, values );

		// Дефолтные настройки для Birthday
		values.put( "ShowMissed", false );
		values.put( "BeforeDays", 15 );
		values.put( "OrderPriority", 20 );
		long BirthdayKindSettingsId=db.insert( "Settings", null, values );

		// Дефолтные настройки для Checklist
		values.put( "ShowMissed", true );
		values.put( "BeforeDays", 1 );
		values.put( "OrderPriority", 30 );
		long ChecklistKindSettingsId=db.insert( "Settings", null, values );

		// Дефолтные настройки для Loan
		values.put( "ShowMissed", true );
		values.put( "BeforeDays", 20 );
		values.put( "OrderPriority", 0 );
		long LoanKindSettingsId=db.insert( "Settings", null, values );
		// endregion

		// region Таблица Kind
		values.clear();
		db.execSQL( _context.getString( R.string.tableKind ) );
		values.put( "Code", Kind.Task.toString() );
		values.put( "Name", _context.getString( R.string.kindNameTasks ) );
		values.put( "SettingsId", ToDoKindSettingsId );
		db.insert( "Kind", null, values );
		values.put( "Code", Kind.Birthday.toString() );
		values.put( "Name", _context.getString( R.string.kindNameBirthday ) );
		values.put( "SettingsId", BirthdayKindSettingsId );
		db.insert( "Kind", null, values );
		values.put( "Code", Kind.Checklist.toString() );
		values.put( "Name", _context.getString( R.string.kindNameChecklist ) );
		values.put( "SettingsId", ChecklistKindSettingsId );
		db.insert( "Kind", null, values );
		values.put( "Code", Kind.Loan.toString() );
		values.put( "Name", _context.getString( R.string.kindNameLoan ) );
		values.put( "SettingsId", LoanKindSettingsId );
		db.insert( "Kind", null, values );
		// endregion

		db.execSQL( _context.getString( R.string.tableGroup ) );
		db.execSQL( _context.getString( R.string.tableItem ) );
		db.execSQL( _context.getString( R.string.tableToDoItem ) );
		db.execSQL( _context.getString( R.string.tableLoan ) );
		db.execSQL( _context.getString( R.string.tableLoanPayment ) );

		if( BuildSettings.FillDatabaseWithTestData )
		{
			logger.info( "Filling the database with test data…" );

			// region Тестовые данные ToDoItem
			values.clear();
			values.put( Item.FieldKindCode, Kind.Task.toString() );
			values.put( Item.FieldSettingsId, ToDoKindSettingsId );
			values.put( Item.FieldDate, "today" );
			values.put( Item.FieldName, "Задача 1" );
			db.insert( Item.Table, null, values );
			values.putNull( Item.FieldDate );
			values.put( Item.FieldName, "Задача 2 very very long text + very very long and mach more long" );
			db.insert( Item.Table, null, values );
			values.put( Item.FieldDate, "2012-06-30 17:00:00.000" );
			values.put( Item.FieldName, "Заполнить расписание!" );
			db.insert( Item.Table, null, values );
			// endregion

			// region Тестовые данные Loan
			Loan loan1=new Loan( "Ипотека (руб.)",             "RUB", 50000, true );
			Loan loan2=new Loan( "Ипотека (доллар)",           "USD", 12300, true );
			Loan loan3=new Loan( "Ипотека (евро)",             "EUR", 7000,  true );
			Loan loan4=new Loan( "Потребительский (МТС Банк)", "RUB", 0,     false );
			LoanWrap loanWrap=new LoanWrap( db );
			loanWrap.Insert( loan1 );
			loanWrap.Insert( loan2 );
			loanWrap.Insert( loan3 );
			loanWrap.Insert( loan4 );

			LoanPayment payment1=new LoanPayment( loan1.getId(), new DateTime( "2014-03-31" ), -25000 );
			LoanPayment payment2=new LoanPayment( loan1.getId(), new DateTime( "2014-04-30" ), -25000 );
			LoanPayment payment3=new LoanPayment( loan1.getId(), new DateTime( "2014-05-27" ), 40000 );
			LoanPayment payment4=new LoanPayment( loan1.getId(), new DateTime( "2014-05-30" ), -25000 );
			LoanPayment payment5=new LoanPayment( loan1.getId(), new DateTime( "2014-06-30" ), -25000 );
			LoanPayment payment6=new LoanPayment( loan1.getId(), new DateTime( "2014-07-31" ), -25000 );
			LoanPaymentWrap paymentWrap=new LoanPaymentWrap( db );
			paymentWrap.Insert( payment1 );
			paymentWrap.Insert( payment2 );
			paymentWrap.Insert( payment3 );
			paymentWrap.Insert( payment4 );
			paymentWrap.Insert( payment5 );
			paymentWrap.Insert( payment6 );
			// endregion
		}
		logger.info( "Database created." );
	}

	@Override
	/** Этот метод вызывается когда обновляется версия БД. */
	public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion )
	{
		logger.info( "Upgrading database from version {} to {}.", oldVersion, newVersion );
	}
}
