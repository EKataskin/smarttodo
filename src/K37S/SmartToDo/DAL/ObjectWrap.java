package k37s.SmartToDo.DAL;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

/** Базовый класс обёрток */
public abstract class ObjectWrap<T> implements IObjectWrap<T>
{
	protected SQLiteDatabase _database;
	protected ContentValues _values=new ContentValues();
	
	public ObjectWrap( SQLiteDatabase database )
	{
		_database=database;
	}

	public abstract long Insert( T obj );
	public abstract void Update( T obj );
	public abstract void Delete( long id );
	public abstract T GetById( long id );
	public abstract List<T> LoadList( String selection, String[] selectionArgs );
}
