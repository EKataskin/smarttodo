package k37s.SmartToDo.DAL;

import java.util.List;

/** Интерфейс обёртки для объектов, хранимых в базе */
public interface IObjectWrap<T>
{
	public long Insert( T obj );
	public void Update( T obj );
	public void Delete( long id );
	public T GetById( long id );
	public List<T> LoadList( String selection, String[] selectionArgs );
}
