package k37s.SmartToDo;

import android.text.format.DateUtils;
import hirondelle.date4j.DateTime;

import java.util.TimeZone;

/** Удобные функции для работы с датой и временем. */
public class DateHelper
{
	// Текущая временная зона.
	private static TimeZone _timeZoneLocal=TimeZone.getDefault();
	// Временная зона UTC.
	private static TimeZone _timeZoneUTC=TimeZone.getTimeZone( "UTC" );

	/**
	 * Возвращает сегодняшнюю дату.
	 * @return Сегодняшняя дата. 
	 */
	public static DateTime Today()
	{
		return DateTime.today( _timeZoneLocal );
	}

	/**
	 * Конвертирует дату в строку с использованием формата текущей локали.
	 * @param date Дата для конвертации.
	 * @return Строка даты в формате текущей локали. 
	 */
	public static String FormatShortDate( DateTime date )
	{
		long millisec=date.getMilliseconds( _timeZoneUTC );
		// If FORMAT_NUMERIC_DATE is set, then the date is shown in numeric format instead of using the name of the month. For example, "12/31/2008" instead of "December 31, 2008".
		// If FORMAT_SHOW_YEAR is set, then the year is always shown. If FORMAT_SHOW_YEAR is not set, then the year is shown only if it is different from the current year.
		return DateUtils.formatDateTime( null, millisec, DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR ); 
	}

	/**
	 * Конвертирует дату в строку (без года) с использованием формата текущей локали.
	 * @param date Дата для конвертации.
	 * @return Строка даты в формате текущей локали. 
	 */
	public static String FormatShortDateNoYear( DateTime date )
	{
		long millisec=date.getMilliseconds( _timeZoneUTC );
		return DateUtils.formatDateTime( null, millisec, DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_NO_YEAR );
	}
}
