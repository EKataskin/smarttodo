package k37s.SmartToDo;

import k37s.SmartToDo.DAL.DatabaseAdapter;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Класс приложения (содержит глобальные переменные). */
public class SmartToDoApp extends Application
{
	private static final Logger logger=LoggerFactory.getLogger( "Application" );
	private static Context _appContext;
	private DatabaseAdapter _dbAdapter;

	@Override
	public void onCreate()
	{
		super.onCreate();
		_appContext=getApplicationContext();
	}

	/**
	 * Возвращает контекст приложения.
	 * @return Контекст приложения
	 */
	public static Context getAppContext()
	{
		return _appContext;
	}

	/**
	 * Возвращает ContentResolver приложения.
	 * @return ContentResolver
	 */
	public static ContentResolver getAppContentResolver()
	{
		return _appContext.getContentResolver();
	}

	/**
	 * Возвращает DatabaseAdapter, единый для всех Activity (singletone).
	 * @return DatabaseAdapter
	 */
	public DatabaseAdapter getDbAdapter()
	{
		if( _dbAdapter==null )
		{
			logger.debug( "Database adapter creating…" );
			// Создаём адаптер для обращения к БД.
			_dbAdapter=new DatabaseAdapter( this );
			_dbAdapter.Open();
			logger.debug( "Database adapter created." );
		}
		return _dbAdapter;
	}

	@Override
	public void onTerminate()
	{
		if( _dbAdapter!=null )
			_dbAdapter.Close();
		super.onTerminate();
	}
}