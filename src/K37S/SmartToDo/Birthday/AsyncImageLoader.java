package k37s.SmartToDo.Birthday;

import k37s.SmartToDo.Business.Contact;
import android.os.AsyncTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** AsyncTask для фоновой загрузки изображений контакта. */
public class AsyncImageLoader extends AsyncTask<Contact, Contact, Void>
{
	private static final Logger logger=LoggerFactory.getLogger( "AsyncImageLoader" );

	@Override
	protected Void doInBackground( Contact... contacts )
	{
//		long startTime=System.currentTimeMillis();
		for( Contact contact : contacts )
		{
			if( isCancelled() )
				return null;
			contact.loadImages();
			publishProgress( contact );
		}
//		long stopTime=System.currentTimeMillis();
//		logger.debug( "PERFORMANCE doInBackground [{}ms]", stopTime-startTime );
		return null;
	}

	@Override
	protected void onProgressUpdate( Contact... contacts )
	{
		for( Contact contact : contacts )
			AsyncLoadHelper.displayContactImages( contact );
	}

	@Override
	protected void onPostExecute( Void aVoid )
	{
		AsyncLoadHelper.resetLoader();
	}
}
