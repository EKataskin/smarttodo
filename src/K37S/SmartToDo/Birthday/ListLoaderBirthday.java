package k37s.SmartToDo.Birthday;

import k37s.SmartToDo.Business.Contact;
import k37s.SmartToDo.SmartToDoApp;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.content.AsyncTaskLoader;
import android.util.TimingLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Loader для загрузки списка контактов с днями рождений.
 * @see "http://www.androiddesignpatterns.com/2012/08/implementing-loaders.html"
 */
public class ListLoaderBirthday extends AsyncTaskLoader<List<Contact>>
{
	private static final Logger logger=LoggerFactory.getLogger( ListLoaderBirthday.class );

	// We hold a reference to the Loader’s data here.
	private List<Contact> _contacts;
	
	private boolean _isLoading;
	public boolean isLoading() { return _isLoading; }

	public ListLoaderBirthday( Context context )
	{
		super( context );
	}

	/** (1) A task that performs the asynchronous load **/
	@Override
	public List<Contact> loadInBackground()
	{
		List<Contact> list=new ArrayList<Contact>();

		TimingLogger timing=new TimingLogger( "TIME", "Contacts loading" );
		Cursor cursor=getContactsWithBirthday();
		int idColumn          =cursor.getColumnIndex( ContactsContract.Contacts._ID );
		int lookupKeyColumn   =cursor.getColumnIndex( ContactsContract.Contacts.LOOKUP_KEY );
		int thumbnailUriColumn=cursor.getColumnIndex( ContactsContract.Contacts.PHOTO_THUMBNAIL_URI );
		int nameColumn        =cursor.getColumnIndex( ContactsContract.Contacts.DISPLAY_NAME_ALTERNATIVE );
		int birthdayColumn    =cursor.getColumnIndex( ContactsContract.CommonDataKinds.Event.START_DATE );

		timing.addSplit( "Cursor loaded" );
		while( cursor.moveToNext() )
		{
			Contact contact=new Contact(
				cursor.getLong( idColumn ),
				cursor.getString( lookupKeyColumn ),
				cursor.getString( nameColumn ),
				cursor.getString( birthdayColumn ),
				cursor.getString( thumbnailUriColumn )
			);
			list.add( contact );
			timing.addSplit( "Contact created" );
		}
		
		Contact.getDefaultPhoto();
		timing.addSplit( "Default photo loaded" );
		
		timing.dumpToLog();
		logger.debug( "Contacts loaded [count={}].", list.size() );
		return list;
	}

	/** (2) Deliver the results to the registered listener **/
	@Override
	public void deliverResult( List<Contact> data )
	{
		logger.debug( "Birthday Loader: deliverResult." );

		_isLoading=false;
		if( isReset() )
		{
			// The Loader has been reset; ignore the result and invalidate the data.
			releaseResources( data );
			return;
		}

		// Hold a reference to the old data so it doesn't get garbage collected.
		// We must protect it until the new data has been delivered.
		List<Contact> oldData=_contacts;
		_contacts=data;

		if( isStarted() )
		{
			// If the Loader is in a started state, deliver the results to the
			// client. The superclass method does this for us.
			super.deliverResult( data );
		}

		// Invalidate the old data as we don't need it any more.
		if( oldData!=null && oldData!=data )
			releaseResources( oldData );
	}

	/** (3) Implement the Loader’s state-dependent behavior **/
	@Override
	protected void onStartLoading()
	{
		logger.debug( "Birthday Loader: onStartLoading." );

		_isLoading=true;
		if( _contacts!=null )
		{
			// Deliver any previously loaded data immediately.
			deliverResult( _contacts );
		}

		if( takeContentChanged() || _contacts==null )
		{
			// When the observer detects a change, it should call onContentChanged()
			// on the Loader, which will cause the next call to takeContentChanged()
			// to return true. If this is ever the case (or if the current data is
			// null), we force a new load.
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading()
	{
		// The Loader is in a stopped state, so we should attempt to cancel the
		// current load (if there is one).
		cancelLoad();
		_isLoading=false;

		// Note that we leave the observer as is. Loaders in a stopped state
		// should still monitor the data source for changes so that the Loader
		// will know to force a new load if it is ever started again.
	}

	@Override
	protected void onReset()
	{
		// Ensure the loader has been stopped.
		onStopLoading();

		// At this point we can release the resources associated with '_loans'.
		if( _contacts!=null )
		{
			releaseResources( _contacts );
			_contacts=null;
		}
	}

	@Override
	public void onCanceled( List<Contact> data )
	{
		// Attempt to cancel the current asynchronous load.
		super.onCanceled( data );

		// The load has been canceled, so we should release the resources
		// associated with 'data'.
		releaseResources( data );
	}

	private void releaseResources( List<Contact> data )
	{
		// For a simple List, there is nothing to do. For something like a Cursor, we
		// would close it in this method. All resources associated with the Loader
		// should be released here.
	}

	/**
	 * Выполняет запрос на чтение всех контактов с указанным днём рождения.
	 * @return курсор с данными контактов.
	 */
	private Cursor getContactsWithBirthday()
	{
		Uri uri=ContactsContract.Data.CONTENT_URI;

		// ContactsContract.Contacts.DISPLAY_NAME             : Имя Отчество Фамилия
		// ContactsContract.Contacts.DISPLAY_NAME_PRIMARY     : Имя Отчество Фамилия
		// ContactsContract.Contacts.DISPLAY_NAME_ALTERNATIVE : Фамилия, Имя Отчество
		String[] projection=new String[]
		{
			ContactsContract.Contacts._ID,
			ContactsContract.Contacts.LOOKUP_KEY,
			ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
			ContactsContract.Contacts.DISPLAY_NAME_ALTERNATIVE,
			ContactsContract.CommonDataKinds.Event.START_DATE
		};

		String where=ContactsContract.Data.MIMETYPE+"=? AND "+ContactsContract.CommonDataKinds.Event.TYPE+"=?";

		String[] selectionArgs=new String[]
		{
			ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE,
			String.valueOf( ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY )
		};

		return SmartToDoApp.getAppContentResolver().query( uri, projection, where, selectionArgs, null );
	}
}