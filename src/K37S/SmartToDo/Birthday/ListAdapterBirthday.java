package k37s.SmartToDo.Birthday;

import k37s.SmartToDo.Business.Contact;
import k37s.SmartToDo.DateHelper;
import k37s.SmartToDo.R;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.*;
import hirondelle.date4j.DateTime;

import java.util.List;

/** Адаптер для списка дней рождения (прослойка между списочным UI и данными). */
public class ListAdapterBirthday extends BaseAdapter
{
//	private static final Logger logger=LoggerFactory.getLogger( ListAdapterBirthday.class );
	
	LayoutInflater _layoutInflater;
	List<Contact> _contacts;

	public void setContacts( List<Contact> contacts )
	{
		_contacts=contacts;
		notifyDataSetChanged();
	}

	ListAdapterBirthday( Context context )
	{
		_layoutInflater=LayoutInflater.from( context );
	}

	@Override
	public int getCount()
	{
		return _contacts==null ? 0 : _contacts.size();
	}

	@Override
	public Object getItem( int position )
	{
		return _contacts==null ? null : _contacts.get( position );
	}

	@Override
	public long getItemId( int position )
	{
		return _contacts==null ? -1 : _contacts.get( position ).getId();
	}

	/** Возвращает заполненный view для элемента списка */
	@Override
	public View getView( int position, View convertView, ViewGroup parent )
	{
		ViewHolder viewHolder;
		
		// Чтобы этот класс выводил что-то в лог, нужно предварительно в консоли сделать: setprop log.tag.TIME VERBOSE
//		TimingLogger timing=new TimingLogger( "TIME", "getView" );
		
		// Используем созданные, но не используемые view.
		View view=convertView;
		if( view==null )
		{
			view=_layoutInflater.inflate( R.layout.row_birthday, parent, false );
			// region ViewHolder
			viewHolder=new ViewHolder();
			viewHolder.contactName=(TextView)view.findViewById( R.id.contact_name );
			viewHolder.birthdayDate=(TextView)view.findViewById( R.id.birthday_date );
			viewHolder.contactBadge=(QuickContactBadge)view.findViewById( R.id.contact_badge );
			viewHolder.zodiacImage=(ImageButton)view.findViewById( R.id.zodiac_img );
			viewHolder.elementImage=(ImageButton)view.findViewById( R.id.element_img );
			viewHolder.fullAge=(TextView)view.findViewById( R.id.full_age );
			view.setTag( viewHolder );
//			timing.addSplit( "Inflate view" );
			// endregion
		}
		else
		{
			viewHolder=(ViewHolder)view.getTag();
		}

		Contact contact=getContact( position );

		// Наименование контакта
		viewHolder.contactName.setText( contact.getName() );

		// region Дата рождения
		DateTime birthDate=contact.getBirthDate();
		if( birthDate!=null )
		{
			// Распознанная дата (с годом или без)
			viewHolder.birthdayDate.setText( birthDate.getYear()<=1900 ? DateHelper.FormatShortDateNoYear( birthDate ) : DateHelper.FormatShortDate( birthDate ) );
			viewHolder.birthdayDate.setTextColor( viewHolder.birthdayDate.getTextColors().getDefaultColor() );
		}
		else
		{
			// Нераспознанная дата
			viewHolder.birthdayDate.setText( contact.getBirthDateStr() );
			viewHolder.birthdayDate.setTextColor( Color.parseColor( "#CC0000" ) );
		}
		// endregion
		
//		viewHolder.fullAge.setText( String.valueOf( viewHolder.getId() ) );

		// Фото
		viewHolder.contactBadge.assignContactUri( contact.getContactUri() );

//		timing.addSplit( "text_fields" );

		// TODO: По тапу на картинках показывать Toast с названием.
		// TODO: Показывать картинку с годом восточного календаря.

		if( contact.imagesLoaded )
		{
			AsyncLoadHelper.removeFromQueue( viewHolder );
			fillViewImages( contact, viewHolder );
		}
		else
		{
			// Предварительно очищаем View.
			clearViewImages( viewHolder );
			AsyncLoadHelper.addToQueue( contact, viewHolder );
		}
//		timing.addSplit( "async_load_initiated" );

//		timing.dumpToLog();
		return view;
	}
	
	public static void fillViewImages( Contact contact, ViewHolder viewHolder )
	{
//		logger.debug( "Contact images displaying: [view={}; contact={}]", viewHolder.getId(), contact.getName() );
		viewHolder.contactBadge.setImageBitmap( contact.getPhoto() );
		viewHolder.zodiacImage.setImageBitmap( contact.getZodiacImage() );
		viewHolder.elementImage.setImageBitmap( contact.getElementImage() );
		if( !contact.showed )
		{
			contact.showed=true;
			// Анимацию для каждого элемента нужно создавать заново, иначе startAnimation будет анимировать все элементы. 
			Animation anim=new AlphaAnimation( 0, 1 );
			anim.setDuration( 300 );
			viewHolder.contactBadge.setAnimation( anim );
			viewHolder.zodiacImage.setAnimation( anim );
			viewHolder.elementImage.setAnimation( anim );
			anim.start();
		}
	}
	
	public static void clearViewImages( ViewHolder viewHolder )
	{
		viewHolder.contactBadge.setImageBitmap( null );
		viewHolder.zodiacImage.setImageBitmap( null );
		viewHolder.elementImage.setImageBitmap( null );
	}

	/**
	 * Возвращает контакт из списка по позиции
	 * @param position позиция в списке
	 * @return контакт
	 */
	Contact getContact( int position )
	{
		return (Contact)getItem( position );
	}
	
	public static class ViewHolder
	{
//		private static int sId;
//		
//		private int mId;
//		public int getId()
//		{
//			return mId;
//		}
//
//		public ViewHolder()
//		{
//			mId=++sId;
//		}

		TextView contactName;
		TextView birthdayDate;
		QuickContactBadge contactBadge;
		ImageButton zodiacImage;
		ImageButton elementImage;
		TextView fullAge;
	}
}