package k37s.SmartToDo.Birthday;

import k37s.SmartToDo.Business.Contact;
import android.support.v4.util.ArrayMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncLoadHelper
{
	private static final Logger logger=LoggerFactory.getLogger( "AsyncLoadHelper" );

	private static ArrayMap<Contact,ListAdapterBirthday.ViewHolder> sContactView=new ArrayMap<Contact, ListAdapterBirthday.ViewHolder>();
	private static AsyncImageLoader mImageLoader;
	private static boolean sScrolled;

	public static void addToQueue( Contact contact, ListAdapterBirthday.ViewHolder viewHolder )
	{
//		long startTime=System.currentTimeMillis();
//		logger.debug( "Async helper contact queued [view={}; contact={}]", viewHolder.getId(), contact.getName() );
		removeFromQueue( viewHolder );
		sContactView.put( contact, viewHolder );
//		long stopTime=System.currentTimeMillis();
//		logger.debug( "PERFORMANCE addToQueue [{}ms]", stopTime-startTime );
		
		startLoading();
	}

	public static void removeFromQueue( ListAdapterBirthday.ViewHolder viewHolder )
	{
		// Каждому контакту должен быть сопоставлен уникальный ViewHolder - поэтому сбрасываем такой же ViewHolder у других контактов.
		for( int i=0; i<sContactView.size(); i++ )
		{
			if( sContactView.valueAt( i )==viewHolder )
				sContactView.setValueAt( i, null );
		}
	}

	private static void startLoading()
	{
//		long startTime=System.currentTimeMillis();
//		logger.debug( "Async helper contact loading called…" );
		// Если находимся в процессе скроллирования, или уже загружаем что-то, или нечего загружать, то выходим.
		if( sScrolled || mImageLoader!=null || sContactView.isEmpty() )
			return;

		Contact[] contacts=sContactView.keySet().toArray( new Contact[sContactView.size()] );
		mImageLoader=new AsyncImageLoader();
		mImageLoader.execute( contacts );
//		logger.debug( "Async helper contact started loading [count={}]", sContactView.size() );
//		long stopTime=System.currentTimeMillis();
//		logger.debug( "PERFORMANCE startLoading [{}ms]", stopTime-startTime );
	}
	
	public static void displayContactImages( Contact contact )
	{
//		long startTime=System.currentTimeMillis();
//		logger.debug( "Async helper contact display called…" );
		// Если находимся в процессе скроллирования, то пропустим отрисовку - сделаем это позже.
		if( sScrolled )
			return;

		ListAdapterBirthday.ViewHolder viewHolder=sContactView.remove( contact );
		if( viewHolder!=null )
		{
			ListAdapterBirthday.fillViewImages( contact, viewHolder );
//			logger.debug( "Async helper contact display [contact={}]", contact.getName() );
		}
//		long stopTime=System.currentTimeMillis();
//		logger.debug( "PERFORMANCE displayContactImages [{}ms]", stopTime-startTime );
		
		startLoading();
	}
	
	public static void setScrollState( boolean scrolled )
	{
		sScrolled=scrolled;
		if( !scrolled )
			startLoading();
	}
	
	public static void resetLoader()
	{
		mImageLoader=null;
		startLoading();
	}
}
