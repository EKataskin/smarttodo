package k37s.SmartToDo.Birthday;

import android.os.Handler;
import k37s.SmartToDo.Business.Contact;
import k37s.SmartToDo.R;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.*;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/** Фрагмент - список дней рождения. */
public class FragmentBirthdayList extends ListFragment
{
	private static final Logger logger=LoggerFactory.getLogger( "FragmentBirthdayList" );

	private ListAdapterBirthday _listAdapter;
	private ListLoaderBirthday _listLoader;
	private ListView _listView;
	private ProgressBar _progressBar;

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		// Загружаем свой кастомный layout списка из xml (вместо дефолтного однострочечного).
		View view=inflater.inflate( R.layout.list_birthday, container, false );
		_progressBar=(ProgressBar)view.findViewById( R.id.list_progress );
		_progressBar.setVisibility( View.GONE );

		_listView=(ListView)view.findViewById( android.R.id.list );
		_listView.setOnScrollListener( new AbsListView.OnScrollListener()
		{
			@Override
			public void onScrollStateChanged( AbsListView absListView, int scrollState )
			{
				AsyncLoadHelper.setScrollState( scrollState!=SCROLL_STATE_IDLE );
			}

			@Override
			public void onScroll( AbsListView absListView, int i, int i2, int i3 ) {}
		} );

		logger.debug( "Birthday List: onCreateView." );
		return view; 
	}

	@Override
	public void onActivityCreated( Bundle savedInstanceState )
	{
		super.onActivityCreated( savedInstanceState );

		// Инициализируем Loader.
		getLoaderManager().initLoader( 0, null, new LoaderCallbacks() );

		_listAdapter=new ListAdapterBirthday( getActivity() );
		setListAdapter( _listAdapter );
	}

	@Override
	public void onResume()
	{
		super.onResume();
		logger.debug( "Birthday List: onResume." );
	}

	@Override
	public void onStart()
	{
		super.onStart();
		// Отображаем ProgressBar при старте фрагмента, но только если данные грузятся в фоне (в этом случае ListView не виден). 
		logger.debug( "Birthday List: onStart." );
		if( _listLoader!=null && _listLoader.isLoading() )
			setProgressVisible( true );
	}
	
	private void setProgressVisible( boolean visible )
	{
		if( _progressBar==null || getView()==null )
			return;
		// Отображаем/скрываем ProgressBar.
		_progressBar.setVisibility( visible ? View.VISIBLE : View.GONE );
	}

	private class LoaderCallbacks implements LoaderManager.LoaderCallbacks<List<Contact>>
	{
		/** Instantiate and return a new Loader for the given ID. */
		public Loader<List<Contact>> onCreateLoader( int i, Bundle bundle )
		{
			// Создаём новый Loader, который будет загружать список кредитов.
			logger.debug( "Birthday List: onCreateLoader." );
			_listLoader=new ListLoaderBirthday( getActivity() );
			return _listLoader;
		}
		
		@Override
		/** Called when a previously created loader has finished its load. */
		public void onLoadFinished( Loader<List<Contact>> loader, List<Contact> contacts )
		{
			// The listview now displays the queried data.
			_listAdapter.setContacts( contacts );

			// Скрываем ProgressBar по окончанию загрузки данных.
			logger.debug( "Birthday List: onLoadFinished." );
			setProgressVisible( false );
			
			// ***** Отладочный код - скроллим ListView туда-сюда *****
			// Execute some code after 2 seconds have passed
			Handler handlerLast = new Handler();
			handlerLast.postDelayed( new Runnable()
			{
				public void run()
				{
					logger.debug( "Scroll to bottom" );
					_listView.smoothScrollToPosition( 14 );
				}
			}, 2000 );

			Handler handlerTop = new Handler();
			handlerTop.postDelayed( new Runnable()
			{
				public void run()
				{
					logger.debug( "Scroll to top" );
					_listView.smoothScrollToPosition( 1 );
				}
			}, 4000 );
		}

		@Override
		/** Called when a previously created loader is being reset, thus making its data unavailable. */
		public void onLoaderReset( Loader<List<Contact>> loader )
		{
			// For whatever reason, the Loader's data is now unavailable.
			// Remove any references to the old data by replacing it with a null Cursor.
			_listAdapter.setContacts( null );

			// Скрываем ProgressBar по окончанию загрузки данных.
			logger.debug( "Birthday List: onLoaderReset." );
			setProgressVisible( false );
		}
	}
}