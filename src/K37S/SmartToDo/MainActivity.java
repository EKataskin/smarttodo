package k37s.SmartToDo;

import k37s.SmartToDo.Birthday.AsyncLoadHelper;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.app.FragmentPagerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Главное окно приложения. */
public class MainActivity extends FragmentActivity
{
	private static final Logger logger=LoggerFactory.getLogger( MainActivity.class );

	// Called when the activity is first created.
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		// Вызываем метод базового класса.
		super.onCreate( savedInstanceState );

		// Натягиваем свой layout.
		setContentView( R.layout.activity_main );
		setTitle( R.string.appName );

		// Включаем отображение табов на ActionBar.
		final ActionBar actionBar=getActionBar();
		actionBar.setNavigationMode( ActionBar.NAVIGATION_MODE_TABS );

		// Реализуем интерфейс, обрабатывающий события пролистывания страниц.
		// ViewPager при смене страницы будет синхронно менять таб на ActionBar.
		ViewPager.OnPageChangeListener viewPagerListener=new ViewPager.OnPageChangeListener()
		{
			@Override
			public void onPageScrolled( int i, float v, int i2 ) {}

			@Override
			public void onPageSelected( int position )
			{
				logger.debug( "Page selected [position={}].", position );
				// When swiping between pages, select the corresponding tab.
				actionBar.setSelectedNavigationItem( position );
			}

			@Override
			public void onPageScrollStateChanged( int state )
			{
				AsyncLoadHelper.setScrollState( state!=ViewPager.SCROLL_STATE_IDLE );
			}
		};

		final ViewPager viewPager=(ViewPager)findViewById( R.id.pager );
		FragmentPagerAdapter pagerAdapter=new MainPagerAdapter( getSupportFragmentManager() );
		viewPager.setAdapter( pagerAdapter );
		viewPager.setOnPageChangeListener( viewPagerListener );

		// Реализуем интерфейс, обрабатывающий события переключения табов.
		// ActionBar при клике на таб будет синхронно менять страницу во ViewPager.
		ActionBar.TabListener tabListener=new ActionBar.TabListener()
		{
			@Override
			public void onTabSelected( ActionBar.Tab tab, FragmentTransaction fragmentTransaction )
			{
				int position=tab.getPosition();
				logger.debug( "Tab selected [position={}].", position );
				// When the tab is selected, switch to the corresponding page in the ViewPager.
				viewPager.setCurrentItem( position );
			}

			@Override
			public void onTabUnselected( ActionBar.Tab tab, FragmentTransaction fragmentTransaction ) {}

			@Override
			public void onTabReselected( ActionBar.Tab tab, FragmentTransaction fragmentTransaction ) {}
		};

		// Создаём табы на ActionBar.
		for( int i=0; i<pagerAdapter.getCount(); i++ )
		{
			ActionBar.Tab tab=actionBar.newTab();
			tab.setText( pagerAdapter.getPageTitle( i ) );
			tab.setTabListener( tabListener );
			actionBar.addTab( tab );
		}
	}
}
