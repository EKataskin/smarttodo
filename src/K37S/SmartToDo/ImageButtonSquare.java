package k37s.SmartToDo;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

// TODO: Оформить класс.

/** ImageButton, который всегда квадратный - ширина устанавливается по высоте */
public class ImageButtonSquare extends ImageButton
{
	public ImageButtonSquare( Context context )
	{
		super( context );
	}

	public ImageButtonSquare( Context context, AttributeSet attrs )
	{
		super( context, attrs );
	}

	public ImageButtonSquare( Context context, AttributeSet attrs, int defStyle )
	{
		super( context, attrs, defStyle );
	}

	@Override
	protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec )
	{
		int height=MeasureSpec.getSize( heightMeasureSpec );
		super.onMeasure(
			MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY ),
			MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY )
		);

//		super.onMeasure( widthMeasureSpec, heightMeasureSpec );
//		width=MeasureSpec.getSize( widthMeasureSpec );
//		height=MeasureSpec.getSize( heightMeasureSpec );
//		if( height==0 )
//		{
//			super.onMeasure( widthMeasureSpec, heightMeasureSpec );
//			return;
//		}
//		super.onMeasure(
//			MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY ),
//			MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY )
//		);
//		super.onMeasure( widthMeasureSpec, heightMeasureSpec );
//		int height=getMeasuredHeight();
//		setMeasuredDimension( height, height );
	}
}
