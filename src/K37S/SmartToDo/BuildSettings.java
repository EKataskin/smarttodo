package k37s.SmartToDo;

/** Настройки для условной компиляции кода. */
public class BuildSettings
{
	/** Флаг: заполнять базу тестовыми данными. */
	public static final boolean FillDatabaseWithTestData=true;
}
