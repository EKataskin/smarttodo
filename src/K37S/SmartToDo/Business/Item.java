package k37s.SmartToDo.Business;

import java.util.Date;

/**
 * Базовый Item.
 * Содержит только общие поля для всех типов Item-ов и используется для
 * быстрого формирования общего списка. По типу можно понять где лежат
 * детализированные данные.
 */
public class Item
{
	public static String Table="Item";

	public static String FieldId="_id";
	private int _id;
	public int getId() { return _id; }
	public void setId( int id ) { _id=id; }

	public static String FieldDate="Date";
	private Date _date;
	public Date getDate() { return _date; }
	public void setDate( Date date ) { _date=date; }

	public static String FieldName="Name";
	private String _name;
	public String getName() { return _name; }
	public void setName( String name ) { _name=name;	}

	public static String FieldNotes="Notes";
	private String _notes;
	public String getNotes() { return _notes; }
	public void setNotes( String notes ) { _notes=notes; }

	public static String FieldKindCode="KindCode";
	private String _kindCode;
	public String getKindCode() { return _kindCode; }
	public void setKindCode( String kindCode ) { _kindCode=kindCode; }

	public static String FieldSettingsId="SettingsId";
}
