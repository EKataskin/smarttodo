package k37s.SmartToDo.Business;

/** Стихия */
public enum ElementSign
{
	Fire,  // Огонь
	Air,   // Воздух
	Earth, // Земля
	Water  // Вода
}
