package k37s.SmartToDo.Business;

/** Знаки зодиака */
public enum ZodiacSign
{
	Aries,       // Овен
	Taurus,      // Телец
	Gemini,      // Близнецы
	Cancer,      // Рак
	Leo,         // Лев
	Virgo,       // Дева
	Libra,       // Весы
	Scorpio,     // Скорпион
	Sagittarius, // Стрелец
	Capricorn,   // Козерог
	Aquarius,    // Водолей
	Pisces       // Рыбы
}
