package k37s.SmartToDo.Business;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import hirondelle.date4j.DateTime;
import k37s.SmartToDo.R;
import k37s.SmartToDo.SmartToDoApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Контакт */
public class Contact
{
	private static final Logger logger=LoggerFactory.getLogger( Contact.class );
	
	private static final Pattern sDatePattern=Pattern.compile( "^(.+)-(.+)-(.+)$" );
	
	private static Hashtable<ZodiacSign,Bitmap> sZodiacBitmaps=new Hashtable<ZodiacSign, Bitmap>( 12 );
	private static Hashtable<ElementSign,Bitmap> sElementBitmaps=new Hashtable<ElementSign, Bitmap>( 4 );

	/** Дефолтное фото для контактов (из ресурсов) */
	private static Bitmap mDefaultPhoto;
	public static Bitmap getDefaultPhoto()
	{
		if( mDefaultPhoto==null )
			mDefaultPhoto=BitmapFactory.decodeResource( SmartToDoApp.getAppContext().getResources(), R.drawable.ic_contact_picture );
		return mDefaultPhoto;
	}
	
	/** ID */
	private long mId;
	public long getId() { return mId; }

	/** An opaque value that contains hints on how to find the contact. */
	private String mLookupKey;
	public String getLookupKey() { return mLookupKey; }

	/** Наименование. */
	private String mName;
	public String getName() { return mName; }

	/** Дата рождения. */
	private DateTime mBirthDate;
	public DateTime getBirthDate() { return mBirthDate; }

	/** Дата рождения. */
	private String mBirthDateStr;
	public String getBirthDateStr() { return mBirthDateStr; }
	
	/**
	 * Uri для получения маленького фото контакта.
	 * PHOTO_THUMBNAIL_URI доступен с API 11 и является более общим способом получить аватар.
	 */
	private Uri mThumbnailUri;
	
	/** Фото контакта. */
	private Bitmap photo;
	public Bitmap getPhoto()
	{
		if( photo==null )
			photo=loadContactPhoto();
		return photo;
	}

	public Bitmap chineseImage;
	public boolean imagesLoaded;
	public boolean imagesLoading;
	public boolean showed;
	
	/** Знак зодиака */
	private ZodiacSign zodiac;
	public ZodiacSign getZodiac() { return zodiac; }
	
	/** Стихия */
	public ElementSign getElement()
	{
		if( zodiac==null )
			return null;
		switch( zodiac )
		{
			case Aries:
			case Leo:
			case Sagittarius:
				return ElementSign.Fire;
			case Gemini:
			case Libra:
			case Aquarius:
				return ElementSign.Air;
			case Taurus:
			case Virgo:
			case Capricorn:
				return ElementSign.Earth;
			case Cancer:
			case Scorpio:
			case Pisces:
				return ElementSign.Water;
			default:
				return null;
		}
	}
	
	public Uri getContactUri()
	{
		return ContactsContract.Contacts.getLookupUri( getId(), getLookupKey() );
	}

	public Contact( long id, String lookupKey, String name, String birthDateStr, String thumbnailUriStr )
	{
		logger.debug( "Contact creating… [id={}; lookup={}; name={}; birthdate={}].", id, mLookupKey, name, birthDateStr );
		mId=id;
		mLookupKey=lookupKey;
		mName=beautifyName( name );
		mBirthDate=parseBirthDate( birthDateStr );
		mBirthDateStr=birthDateStr;
		mThumbnailUri=thumbnailUriStr!=null ? Uri.parse( thumbnailUriStr ) : null;
		if( mBirthDate!=null )
			zodiac=getZodiacSign( mBirthDate.getDay(), mBirthDate.getMonth() );
	}
	
	private String beautifyName( String name )
	{
		// Избавляемся от запятой: "Фамилия, Имя Отчество".
		return name.replaceAll( ",", "" );
	}
	
	private DateTime parseBirthDate( String birthDateStr )
	{
		Matcher dateMatcher=sDatePattern.matcher( birthDateStr );
		if( !dateMatcher.matches() )
		{
			logger.warn( "Can't parse (regex) birth date string '{}'!", birthDateStr );
			return null;
		}

		// Дата без года (--MM-dd)
		if( dateMatcher.groupCount()==3 && dateMatcher.group( 1 ).equals( "-" ) )
			birthDateStr=birthDateStr.replace( "--", "0001-" );
			
		if( DateTime.isParseable( birthDateStr ) )
			return new DateTime( birthDateStr );
		else
		{
			logger.warn( "Can't parse birth date string '{}'!", birthDateStr );
			return null;
		}
	}

	/**
	 * Вычисляет знак зодиака по дате рождения
	 * @param day день даты рождения
	 * @param month месяц даты рождения
	 * @return знак зодиака ZodiacSign 
	 */
	private ZodiacSign getZodiacSign( int day, int month )
	{
		switch( month )
		{
			case 1 : return day<20 ? ZodiacSign.Capricorn   : ZodiacSign.Aquarius;
			case 2 : return day<19 ? ZodiacSign.Aquarius    : ZodiacSign.Pisces;
			case 3 : return day<21 ? ZodiacSign.Pisces      : ZodiacSign.Aries;
			case 4 : return day<20 ? ZodiacSign.Aries       : ZodiacSign.Taurus;
			case 5 : return day<21 ? ZodiacSign.Taurus      : ZodiacSign.Gemini;
			case 6 : return day<21 ? ZodiacSign.Gemini      : ZodiacSign.Cancer;
			case 7 : return day<23 ? ZodiacSign.Cancer      : ZodiacSign.Leo;
			case 8 : return day<23 ? ZodiacSign.Leo         : ZodiacSign.Virgo;
			case 9 : return day<23 ? ZodiacSign.Virgo       : ZodiacSign.Libra;
			case 10: return day<23 ? ZodiacSign.Libra       : ZodiacSign.Scorpio;
			case 11: return day<22 ? ZodiacSign.Scorpio     : ZodiacSign.Sagittarius;
			case 12: return day<22 ? ZodiacSign.Sagittarius : ZodiacSign.Capricorn;
		}
		return null;
	}

	/** Картинка знака зодиака. */
	public Bitmap getZodiacImage()
	{
		if( zodiac==null )
			return null;
		return sZodiacBitmaps.get( zodiac );
	}

	/** Картинка стихии. */
	public Bitmap getElementImage()
	{
		ElementSign element=getElement();
		if( element==null )
			return null;
		return sElementBitmaps.get( element );
	}

	/**
	 * Загружает фото контакта.
	 * Сначала пытается получить фото через PHOTO_THUMBNAIL_URI, затем через LOOKUP_URI.
	 * Если не удалось загрузить - используется дефолтное фото из ресурсов.
	 * @return Bitmap с фото контакта.
	 */
	private Bitmap loadContactPhoto()
	{
		// Первая попытка - пытаемся получить фото контакта через PHOTO_THUMBNAIL_URI.
		// PHOTO_THUMBNAIL_URI доступен с API 11 и является более общим способом получить аватар.
		// Если PHOTO_ID не null, то и PHOTO_THUMBNAIL_URI не null. Но нам не нужна большая фотка.
		if( mThumbnailUri!=null )
		{
			Cursor thumbnailCursor=SmartToDoApp.getAppContentResolver().query( mThumbnailUri, new String[] { ContactsContract.Contacts.Photo.PHOTO }, null, null, null );
			byte[] photoBytes=null;
			try
			{
				if( thumbnailCursor.moveToFirst() )
					photoBytes=thumbnailCursor.getBlob( 0 );
				if( photoBytes!=null )
					return BitmapFactory.decodeByteArray( photoBytes, 0, photoBytes.length );
			}
			catch( Exception ex )
			{
				// TODO: Handle exception.
				ex.printStackTrace();
			}
			finally
			{
				thumbnailCursor.close();
			}
		}

		// Вторая попытка - пытаемся получить фото контакта через метод openContactPhotoInputStream. 
		InputStream photoStream=ContactsContract.Contacts.openContactPhotoInputStream( SmartToDoApp.getAppContentResolver(), getContactUri() );
		if( photoStream!=null )
			return BitmapFactory.decodeStream( photoStream );

		// Если все попытки не увенчались успехом, то используем дефолтную картинку.
		return getDefaultPhoto();
	}

	public void loadImages()
	{
		if( imagesLoaded || imagesLoading )
			return;
		imagesLoading=true;

		getPhoto();

		// region Знак зодиака
		if( zodiac!=null && !sZodiacBitmaps.containsKey( zodiac ) )
		{
			int zodiacId;
			switch( zodiac )
			{
				case Aries       : zodiacId=R.drawable.zodiac_aries; break;
				case Taurus      : zodiacId=R.drawable.zodiac_taurus; break;
				case Gemini      : zodiacId=R.drawable.zodiac_gemini; break;
				case Cancer      : zodiacId=R.drawable.zodiac_cancer; break;
				case Leo         : zodiacId=R.drawable.zodiac_leo; break;
				case Virgo       : zodiacId=R.drawable.zodiac_virgo; break;
				case Libra       : zodiacId=R.drawable.zodiac_libra; break;
				case Scorpio     : zodiacId=R.drawable.zodiac_scorpion; break;
				case Sagittarius : zodiacId=R.drawable.zodiac_sagittarius; break;
				case Capricorn   : zodiacId=R.drawable.zodiac_capricorn; break;
				case Aquarius    : zodiacId=R.drawable.zodiac_aquarius; break;
				case Pisces      : zodiacId=R.drawable.zodiac_pisces; break;
				default          : zodiacId=-1; break;
			}
			if( zodiacId>0 )
			{
//				Bitmap zodiacImage=BitmapFactory.decodeResource( SmartToDoApp.getAppContext().getResources(), zodiacId );
				Bitmap zodiacImage=downscaleBitmapUsingDensities( 32, zodiacId );
				sZodiacBitmaps.put( zodiac, zodiacImage );
			}
		}
		// endregion

		// region Стихия
		ElementSign element=getElement();
		if( element!=null && !sElementBitmaps.containsKey( element ) )
		{
			int elementId;
			switch( element )
			{
				case Fire:  elementId=R.drawable.element_fire; break;
				case Air:   elementId=R.drawable.element_air; break;
				case Earth: elementId=R.drawable.element_earth; break;
				case Water: elementId=R.drawable.element_water; break;
				default:    elementId=-1; break;
			}
			if( elementId>0 )
			{
//				Bitmap elementImage=BitmapFactory.decodeResource( SmartToDoApp.getAppContext().getResources(), elementId );
				Bitmap elementImage=downscaleBitmapUsingDensities( 32, elementId );
				sElementBitmaps.put( element, elementImage );
			}
		}
		// endregion

		imagesLoading=false;
		imagesLoaded=true;
	}
	
	/**
	 * Код взят отсюда: http://stackoverflow.com/questions/16408505/how-to-downsample-images-correctly
	 * Но что-то отвратно выглядят сжатые картинки.
	 * */
	private Bitmap downscaleBitmapUsingDensities( final int sampleSize, final int imageResId )
	{
		final BitmapFactory.Options bitmapOptions=new BitmapFactory.Options();

		// as much as possible, use google's way to downsample:
		bitmapOptions.inSampleSize=1;
		bitmapOptions.inDensity=1;
		bitmapOptions.inTargetDensity=1;
		while( bitmapOptions.inSampleSize*2<=sampleSize )
			bitmapOptions.inSampleSize*=2;

		// if google's way to downsample isn't enough, do some more :
		if( bitmapOptions.inSampleSize!=sampleSize )
		{
			// downsample by bitmapOptions.inSampleSize/originalSampleSize .
			bitmapOptions.inTargetDensity=bitmapOptions.inSampleSize;
			bitmapOptions.inDensity=sampleSize;
		}
		else if( sampleSize==1 )
		{
			bitmapOptions.inTargetDensity=1;
			bitmapOptions.inDensity=sampleSize;
		}

		final Bitmap scaledBitmap=BitmapFactory.decodeResource( SmartToDoApp.getAppContext().getResources(), imageResId, bitmapOptions );
		scaledBitmap.setDensity( Bitmap.DENSITY_NONE );
		return scaledBitmap;
	}
}
