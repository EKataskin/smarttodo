package k37s.SmartToDo.Business;

import k37s.SmartToDo.DateHelper;
import hirondelle.date4j.DateTime;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/** Кредит. */
public class Loan
{
	private long _id;
	public long getId() { return _id; }
	public void setId( long id ) { _id=id; }

	/** Наименование. */
	private String _name;
	public String getName() { return _name; }

	/** Валюта. */
	private String _currencyCode;
	public String getCurrencyCode() { return _currencyCode; }
	public Currency getCurrency() { return Currency.getInstance( _currencyCode ); }

	/** Начальный баланс расчётного счёта. */
	private float _initialbalance;
	public float getInitialBalance() { return _initialbalance; }

	/**
	 * Возвращает баланс на указанную дату (на конец дня).
	 * Баланс считается с учётом всех платежей, совершаемых в эту дату.
	 * @param date Дата, на которую нужно вычислить баланс.
	 * @return Баланс на дату.
	 */
	public float getBalance( DateTime date )
	{
		if( !_useBalance )
			return 0;
		float balance=_initialbalance;
		for( LoanPayment payment : _payments )
			if( payment.getDate().lteq( date ) )
				balance+=payment.getSum();
		return balance;
	}

	/** Возвращает баланс на текущую дату. */
	public float getBalance()
	{
		return getBalance( DateHelper.Today() );
	}

	/**
	 * Признак кредита с балансом.
	 * В случае false баланс не ведётся. Считается что все деньги, положенные на счёт, списываются при очередном погашении.
	 */
	private boolean _useBalance;
	public boolean isUseBalance() { return _useBalance; }

	/**
	 * Список платежей.
	 * Список должен быть отсортирован по датам (сортировку делает враппер).
	 */
	private List<LoanPayment> _payments=new ArrayList<LoanPayment>();
	public List<LoanPayment> getPayments() { return _payments; }

	public Loan( long id, String name, String currencyCode, float initialBalance, boolean useBalance )
	{
		_id=id;
		_name=name;
		_currencyCode=currencyCode;
		_initialbalance=initialBalance;
		_useBalance=useBalance;
	}

	public Loan( String name, String currencyCode, float initialBalance, boolean useBalance )
	{
		this( 0, name, currencyCode, initialBalance, useBalance );
	}

	/**
	 * Вычисляет дату и сумму следующего пополнения.
	 * @return Платеж с крайней датой и минимально-необходимой суммой.
	 */
	public LoanPayment getNextPayment()
	{
		DateTime today=DateHelper.Today();
		LoanPayment payment=new LoanPayment( _id, today, 0 );
		float balance=getBalance();
		
		// Если баланс на 'сегодня' уже отрицательный, то вычислять что-либо дальше бессмысленно.
		if( balance<0 )
		{
			payment.setSum( balance );
			payment.setDate( today );
			return payment;
		}
		
		// Ищем ближайшую в будущем дату платежа, когда баланс станет отрицательным.
		for( LoanPayment p : _payments )
		{
			if( p.getDate().lteq( today ) )
				continue;

			balance+=p.getSum();
			if( balance<0 )
			{
				payment.setSum( balance );
				payment.setDate( p.getDate() );
				return payment;
			}
		}
		
		// Если не нашли дату падения баланса ниже 0, то вернём 'пустой' платеж.
		return payment;
	}
}
