package k37s.SmartToDo.Business;

/**
 * Приоритет задачи.
 */
public enum Priority
{
	Highest( 10 ),
	High( 7 ),
	Normal( 5 ),
	Low( 3 ),
	Lowest( 1 );

	private final int Value;
	public int getValue() { return Value; }

	Priority( int value )
	{
		Value=value;
	}

}
