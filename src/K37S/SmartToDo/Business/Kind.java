package k37s.SmartToDo.Business;

/**
 * Тип события.
 * Task     - Задачи
 * Shopping - Покупки
 * Birthday - Дни рождений
 * Loan     - Кредиты
 */
public enum Kind
{
	Task,
	Checklist,
	Birthday,
	Loan
}
