package k37s.SmartToDo.Business;

import hirondelle.date4j.DateTime;

import java.util.Comparator;

/** Платёж по кредиту. */
public class LoanPayment
{
	private long _id;
	public long getId() { return _id; }
	public void setId( long id ) { _id=id; }

	/** Ссылка на кредит. */
	private long _loanId;
	public long getLoanId() { return _loanId; }
	public void setLoanId( long loanId ) { _loanId=loanId; }

	/** Дата платежа. */
	private DateTime _date;
	public DateTime getDate() { return _date; }
	public void setDate( DateTime date ) { _date=date; }

	/** Сумма платежа. */
	private float _sum;
	public float getSum() { return _sum; }
	public void setSum( float sum ) { _sum=sum; }

	/** Комментарий к платежу. */
	private String _comment;
	public String getComment() { return _comment; }
	public void setComment( String comment ) { _comment=comment; }

	public LoanPayment( long id, long loanId, DateTime date, float sum, String... comment )
	{
		_id=id;
		_loanId=loanId;
		_date=date;
		_sum=sum;
		if( comment.length>0 )
			_comment=comment[0];
	}

	public LoanPayment( long loanId, DateTime date, float sum, String... comment )
	{
		this( 0, loanId, date, sum, comment );
	}

	public static Comparator<LoanPayment> COMPARE_BY_DATE=new Comparator<LoanPayment>()
	{
		@Override
		public int compare( LoanPayment one, LoanPayment other )
		{
			return one._date.compareTo( other._date );
		}
	};
}
